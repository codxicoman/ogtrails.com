// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require ela_admin/lib/jquery/jquery.min.js
//= require ela_admin/lib/jquery-ui/jquery-ui.min.js
//= require ela_admin/lib/jquery-ui/jquery.ui.touch-punch.min.js
//= require ela_admin/lib/bootstrap/js/popper.min.js
//= require ela_admin/lib/bootstrap/js/bootstrap.min.js
//= require ela_admin/jquery.slimscroll.js
//= require ela_admin/sidebarmenu.js
//= require ela_admin/lib/sticky-kit-master/dist/sticky-kit.min.js
//= require ela_admin/custom.min.js
//= require_tree ./ela_admin
//= require ckeditor/init
// require_tree .
