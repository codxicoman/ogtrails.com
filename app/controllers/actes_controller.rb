class ActesController < ApplicationController
  def show
    @acte = Acte.find(params[:id])
  end

  # GET /Actes/new
  def new
    @programme = Programme.find(params[:programme_id])
    @acte = @programme.actes.build

  end

  # GET /Actes/1/edit
  def edit
    @acte = Acte.find(params[:id])
  end

  # POST /Actes
  # POST /Actes.json
  def create

    @acte = Acte.new(acte_params)

    respond_to do |format|
      if @acte.save
        format.html { redirect_to programmes_path, notice: 'Cet acte a été créé avec succès' }
        format.json { render :show, status: :created, location: @acte }
      else
        format.html { render :new }
        format.json { render json: @programmes.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /Actes/1
  # PATCH/PUT /Actes/1.json
  def update
    respond_to do |format|
      @acte = Acte.find(params[:id])
      if @acte.update(acte_params)
        format.html { redirect_to programmes_path, notice: 'Cet acte a été mis à jour' }
        format.json { render :show, status: :ok, location: @acte }
      else
        format.html { render :edit }
        format.json { render json: programmes.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /Actes/1
  # DELETE /Actes/1.json
  def destroy
    @acte = Acte.find(params[:id])
    @acte.destroy
    respond_to do |format|
      format.html { redirect_to programmes_path, notice: 'Cet acte a été supprimé' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_acte
      @acte = Acte.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def acte_params
      params.require(:acte).permit(:nom, :programme_id)
    end
end

