class ActivitesController < ApplicationController
  def show
    @activite = Activite.find(params[:id])
  end

  # GET /ResultatAttenus/new
  def new
    @activite = Activite.new
    @activite.resultat_attenu_id = params[:resultat_attenu_id]
  end

  # GET /ResultatAttenus/1/edit
  def edit
    @activite = Activite.find(params[:id])
  end

  # POST /ResultatAttenus
  # POST /ResultatAttenus.json
  def create

    @activite = Activite.new(activite_params)

    respond_to do |format|
      if @activite.save
        format.html { redirect_to programmes_path, notice: 'Cet activité a été créé avec succès' }
        format.json { render :show, status: :created, location: @activite }
      else
        format.html { render :new }
        format.json { render json: @programmes.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ResultatAttenus/1
  # PATCH/PUT /ResultatAttenus/1.json
  def update
    respond_to do |format|
      @activite = Activite.find(params[:id])
      if @activite.update(activite_params)
        format.html { redirect_to programmes_path, notice: 'Cet activité a été mis à jour' }
        format.json { render :show, status: :ok, location: @activite }
      else
        format.html { render :edit }
        format.json { render json: programmes.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ResultatAttenus/1
  # DELETE /ResultatAttenus/1.json
  def destroy
    @activite = Activite.find(params[:id])
    @activite.destroy
    respond_to do |format|
      format.html { redirect_to programmes_path, notice: 'Cet activité a été supprimé' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_activite
      @activite = Activite.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def activite_params
      params.require(:activite).permit(:nom, :description, :resultat_attenu_id, :debut, :fin, :date_activite)
    end
end
