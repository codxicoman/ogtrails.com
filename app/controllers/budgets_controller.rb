class BudgetsController < ApplicationController
# GET /programmes/1/edit
  def edit
    @budget = Budget.find(params[:id])
  end

  # PATCH/PUT /programmes/1
  # PATCH/PUT /programmes/1.json
  def update
    respond_to do |format|
      @budget = Budget.find(params[:id])
      if @budget.update(budget_params)
        format.html { redirect_to edit_budget_path(@budget), notice: 'Cet Budget a été mis à jour avec succès' }
        format.json { render :show, status: :ok, location: @budget }
      else
        format.html { render :edit }
        format.json { render json: @programme.errors, status: :unprocessable_entity }
      end
    end
  end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_programme
      @budget = Budget.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def budget_params
      params.require(:budget).permit(:prevision, :trimestre1, :trimestre2, :trimestre3, :trimestre4)
    end
end
