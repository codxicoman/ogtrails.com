class CommentairesController < ApplicationController
  # GET /programmes/new
  def new
    @commentaire = Commentaire.new
    @commentaire.matrice_id = params[:matrice_id]

  end

  # POST /programmes
  # POST /programmes.json
  def create
    @commentaire = Commentaire.new(commentaire_params)

    respond_to do |format|
      if @commentaire.save
        format.html { redirect_to programmes_path, notice: 'Le programme a été créé avec succès' }
        format.json { render :show, status: :created, location: @commentaire }
      else
        format.html { render :new }
        format.json { render json: @commentaire.errors, status: :unprocessable_entity }
      end
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_commentaire
      @commentaire = Commentaire.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def commentaire_params
      params.require(:commentaire).permit(:content, :matrice_id)
    end
end
