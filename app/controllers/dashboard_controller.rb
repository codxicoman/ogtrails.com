class DashboardController < ApplicationController
  def index
    @ministere = Ministere.all.load
    @programmes = Programme.all.load
    @titres = Titre.all.load
    @actionss = Acte.joins(:programme).load
    @resultat_attendu = ResultatAttenu.joins(:acte).load
    @activite = Activite.joins(:resultat_attenu).load
    @indicateur = Indicateur.joins(:activite).load
    @budget = Budget.all.load
  end

  def diagramme
  end
end
