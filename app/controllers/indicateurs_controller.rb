class IndicateursController < ApplicationController

  def show
    @indicateur = Indicateur.find(params[:id])
  end

  # GET /programmes/new
  def new
    @indicateur = Indicateur.new
    @indicateur.activite_id = params[:activite_id]

  end

  # GET /programmes/1/edit
  def edit
    @indicateur = Indicateur.find(params[:id])
  end

  # POST /programmes
  # POST /programmes.json
  def create
    @indicateur = Indicateur.new(programme_params)

    respond_to do |format|
      if @indicateur.save
        format.html { redirect_to programmes_path, notice: 'Le programme a été créé avec succès' }
        format.json { render :show, status: :created, location: @indicateur }
      else
        format.html { render :new }
        format.json { render json: @indicateur.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /programmes/1
  # PATCH/PUT /programmes/1.json
  def update
    respond_to do |format|
      if @indicateur.update(programme_params)
        format.html { redirect_to programmes_path, notice: 'Le programme a été mis à jour' }
        format.json { render :show, status: :ok, location: @indicateur }
      else
        format.html { render :edit }
        format.json { render json: @indicateur.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /programmes/1
  # DELETE /programmes/1.json
  def destroy
    @indicateur.destroy
    respond_to do |format|
      format.html { redirect_to programmes_url, notice: 'Le programme a été supprimé' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_programme
      @indicateur = Indicateur.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def programme_params
      params.require(:indicateur).permit(:nom, :description, :activite_id)
    end
end
