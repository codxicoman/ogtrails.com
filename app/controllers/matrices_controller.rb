class MatricesController < ApplicationController
  def show
    @matrice = Matrice.all
    @indicateur = Indicateur.find(params[:id])
    @commentaire = Commentaire.all
  end

  # GET /ResultatAttenus/new
  def new
    @matrice = Matrice.new
    @matrice.indicateur_id = params[:indicateur_id]
  end

  # GET /ResultatAttenus/1/edit
  def edit
    @matrice = Matrice.find(params[:id])
  end

  # POST /ResultatAttenus
  # POST /ResultatAttenus.json
  def create

    @matrice = Matrice.new(activite_params)

    respond_to do |format|
      if @matrice.save
        format.html { redirect_to programmes_path, notice: 'Cette matrice a été créé avec succès' }
        format.json { render :show, status: :created, location: @matrice }
      else
        format.html { render :new }
        format.json { render json: @programmes.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ResultatAttenus/1
  # PATCH/PUT /ResultatAttenus/1.json
  def update
    respond_to do |format|
      @matrice = Matrice.find(params[:id])
      if @matrice.update(matrice_params)
        format.html { redirect_to matrix_path(@matrice.id), notice: 'Cette matrice a été mis à jour' }
        format.json { render :show, status: :ok, location: @matrice }
      else
        format.html { render :edit }
        format.json { render json: programmes.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ResultatAttenus/1
  # DELETE /ResultatAttenus/1.json
  def destroy
    @matrice = Matrice.find(params[:id])
    @matrice.destroy
    respond_to do |format|
      format.html { redirect_to programmes_path, notice: 'Cet activité a été supprimé' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_matrice
      @matrice = Matrice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def matrice_params
      params.require(:matrice).permit(:annee, :valeur_initiale, :prevision, :resultat, :ecart, :indicateur_id)
    end

end
