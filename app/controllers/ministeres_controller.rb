class MinisteresController < ApplicationController

	def index
    @ministere = Ministere.all
	end

	def new
		@ministere = Ministere.new
	end

  def show
    @ministere = Ministere.find(params[:id])
  end

  def edit
    @ministere = Ministere.find params[:id]
  end

	def create
    respond_to do |format|
    @ministere = Ministere.new(ministere_params)
      if @ministere.save
        format.html { redirect_to ministeres_path, notice: 'Le ministere a été créé' }
        format.json { head :no_content }
      else
        format.json { render json: @ministere.errors, status: :unprocessable_entity }
      end
    end
	end

  def update
    respond_to do |format|
      @ministere = Ministere.find(params[:id])
      if @ministere.update(ministere_params)
        format.html { redirect_to ministeres_path, notice: 'La ministere a été mis à jour' }
        format.json { render :show, status: :ok, location: @ministere }
      else
        format.html { render :edit }
        format.json { render json: @ministere.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @ministere = Ministere.find(params[:id])
    @ministere.destroy
    respond_to do |format|
      format.html { redirect_to ministeres_path, notice: 'La ministere a été supprimé' }
      format.json { head :no_content }
    end
  end

    def ministere_params
    	params.require(:ministere).permit(:nom, :description, :ministre, :numero, :email)
 	end
end
