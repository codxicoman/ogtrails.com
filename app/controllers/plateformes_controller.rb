class PlateformesController < ApplicationController
  def index
    @plateforme = Plateforme.all
  end

  def new
    @plateforme = Plateforme.new
  end

  def edit
    @plateforme = Plateforme.find params[:id]
  end

  def show
    @plateforme = Plateforme.find(params[:id])
  end

  def create
    respond_to do |format|
    @plateforme = Plateforme.new(plateforme_params)
      if @plateforme.save
        format.html { redirect_to plateformes_path, notice: 'La plateforme a été créée' }
        format.json { head :no_content }
      else
        format.json { render json: @plateforme.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @plateforme = Plateforme.find(params[:id])
    @plateforme.destroy
    respond_to do |format|
      format.html { redirect_to plateformes_path, notice: 'La plateforme a été supprimée' }
      format.json { head :no_content }
    end
  end

  def update
    respond_to do |format|
      @plateforme = Plateforme.find(params[:id])
      if @plateforme.update(plateforme_params)
        format.html { redirect_to plateformes_path, notice: 'La plateforme a été mise à jour' }
        format.json { render :show, status: :ok, location: @plateforme }
      else
        format.html { render :edit }
        format.json { render json: @plateforme.errors, status: :unprocessable_entity }
      end
    end
  end

  def plateforme_params
      params.require(:plateforme).permit(:nom, :siege, :ministere_id, :email, :numero, :description)
  end
end
