class ResultatAttenusController < ApplicationController
  def show
    @resultatatttenu = ResultatAttenu.find(params[:id])
  end

  # GET /ResultatAttenus/new
  def new
    @acte = Acte.find(params[:acte_id])
    @resultatattenu = ResultatAttenu.new
    @resultatattenu.acte_id = params[:acte_id]
  end

  # GET /ResultatAttenus/1/edit
  def edit
    @resultatatttenu = ResultatAttenu.find(params[:id])
  end

  # POST /ResultatAttenus
  # POST /ResultatAttenus.json
  def create

    @resultatatttenu = ResultatAttenu.new(resultatattenu_params)

    respond_to do |format|
      if @resultatatttenu.save
        format.html { redirect_to programmes_path, notice: 'Cet ResultatAttenu a été créé avec succès' }
        format.json { render :show, status: :created, location: @resultatatttenu }
      else
        format.html { render :new }
        format.json { render json: @programmes.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ResultatAttenus/1
  # PATCH/PUT /ResultatAttenus/1.json
  def update
    respond_to do |format|
      @resultatatttenu = ResultatAttenu.find(params[:id])
      if @resultatatttenu.update(resultatattenu_params)
        format.html { redirect_to programmes_path, notice: 'Cet ResultatAttenu a été mis à jour' }
        format.json { render :show, status: :ok, location: @resultatatttenu }
      else
        format.html { render :edit }
        format.json { render json: programmes.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ResultatAttenus/1
  # DELETE /ResultatAttenus/1.json
  def destroy
    @resultatatttenu = ResultatAttenu.find(params[:id])
    @resultatatttenu.destroy
    respond_to do |format|
      format.html { redirect_to programmes_path, notice: 'Cet ResultatAttenu a été supprimé' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_resultatattenu
      @resultatatttenu = ResultatAttenu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def resultatattenu_params
      params.require(:resultat_attenu).permit(:nom, :description, :acte_id)
    end
end
