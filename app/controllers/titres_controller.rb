class TitresController < ApplicationController
  before_action :set_titre, only: [:show, :edit, :update, :destroy]

  # GET /titres
  # GET /titres.json
  def index
    @ministere = Ministere.where('id = ?', 1).load
    @titres = Titre.all.load
    @budget = Budget.all.load
  end

  # GET /titres/1
  # GET /titres/1.json
  def show
     @titre = Titre.find(params[:id])
  end

  # GET /titres/new
  def new
    @titre = Titre.new
  end

  # GET /titres/1/edit
  def edit
      @titre = Titre.find(params[:id])
  end

  # POST /programmes
  # POST /programmes.json
  def create
    @titre = Titre.new(titre_params)

    respond_to do |format|
      if @titre.save
        format.html { redirect_to titres_path, notice: 'Le programme a été créé avec succès' }
        format.json { render :show, status: :created, location: @programme }
      else
        format.html { render :new }
        format.json { render json: @titre.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /titres/1
  # PATCH/PUT /titres/1.json
  def update
    respond_to do |format|
      if @titre.update(titre_params)
        format.html { redirect_to titres_path, notice: 'Titre was successfully updated.' }
        format.json { render :show, status: :ok, location: @titre }
      else
        format.html { render :edit }
        format.json { render json: @titre.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /titres/1
  # DELETE /titres/1.json
  def destroy
    @titre.destroy
    respond_to do |format|
      format.html { redirect_to titres_url, notice: 'Titre was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_titre
      @titre = Titre.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def titre_params
      params.require(:titre).permit(:nom, :description, :ministere_id)
    end
end
