class Acte < ApplicationRecord
	belongs_to :programme
	has_many :resultat_attenu

  rails_admin do
      label "Action"
      label_plural "Actions"
      list do
          field :nom
          field :description
          field :programme  do
            label "Programme"
          end
          field :created_at
          field :updated_at
      end
      edit do
          field :nom
          field :description, :ck_editor
          field :programme do
            label "Programme"
          end
      end
  end
end
