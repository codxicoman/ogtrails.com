class Activite < ApplicationRecord
	belongs_to :resultat_attenu
	has_one :indicateur
	has_many :budgets, as: :budgetable
end
