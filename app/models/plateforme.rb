class Plateforme < ApplicationRecord
  belongs_to :ministere
  has_many :user

  has_many :thematique_plateformes
  has_many :thematiques, through: :thematique_plateformes
end
