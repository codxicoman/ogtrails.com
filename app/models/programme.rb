class Programme < ApplicationRecord
  belongs_to :ministere
  has_many :actes
  has_many :budgets, as: :budgetable
  has_many :phases

  has_many :thematique_programmes
  has_many :thematiques, through: :thematique_programmes

  rails_admin do
      label "Programme"
      label_plural "Programmes"
      list do
          field :nom
          field :description
          field :ministere  do
            label "Ministere"
          end
          field :created_at
          field :updated_at
      end
      edit do
          field :nom
          field :description, :ck_editor
          field :ministere do
            label "Ministere"
          end
      end
  end
end
