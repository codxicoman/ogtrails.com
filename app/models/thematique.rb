class Thematique < ApplicationRecord
  has_many :thematique_programmes
  has_many :programmes, through: :thematique_programmes

  has_many :thematique_plateformes
  has_many :plateformes, through: :thematique_plateformes
end
