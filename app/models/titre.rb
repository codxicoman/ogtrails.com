class Titre < ApplicationRecord
  belongs_to :ministere
  #has_and_belongs_to_many :budgets
  has_many :budgets, as: :budgetable
end
