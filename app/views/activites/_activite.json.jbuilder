json.extract! activite, :id, :created_at, :updated_at
json.url activite_url(activite, format: :json)
