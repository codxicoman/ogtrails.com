Rails.application.routes.draw do

  get 'graphiques/diagramme'

  get 'graphiques/courbe'

  get 'budgets/edit'

  get 'commentaires/new'

  mount Ckeditor::Engine => '/ckeditor'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root "dashboard#index"
  get 'dashboard/index'

  resources :ministeres
  resources :plateformes
  resources :programmes
  resources :titres
  resources :actes
  resources :resultat_attenus
  resources :activites
  resources :indicateurs
  resources :budgets
  resources :matrices
  resources :commentaires
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
