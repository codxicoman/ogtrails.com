class CreateMinisteres < ActiveRecord::Migration[5.1]
  def change
    create_table :ministeres do |t|
      t.string :nom
      t.string :description
      t.string :ministre

      t.timestamps
    end
  end
end
