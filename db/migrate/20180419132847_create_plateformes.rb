class CreatePlateformes < ActiveRecord::Migration[5.1]
  def change
    create_table :plateformes do |t|
      #t.references :ministere, foreign_key: true
      t.string :nom
      t.string :siege

      t.timestamps
    end
  end
end
