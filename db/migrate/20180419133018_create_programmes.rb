class CreateProgrammes < ActiveRecord::Migration[5.1]
  def change
    create_table :programmes do |t|
      t.string :nom
      #t.references :ministere, foreign_key: true
      t.text :description

      t.timestamps
    end
  end
end
