class CreatePeriodes < ActiveRecord::Migration[5.1]
  def change
    create_table :periodes do |t|
      t.string :libelle
      t.datetime :date_debut
      t.datetime :date_fin

      t.timestamps
    end
  end
end
