class CreateResultatAttenus < ActiveRecord::Migration[5.1]
  def change
    create_table :resultat_attenus do |t|
      t.string :nom
      t.text :description

      t.timestamps
    end
  end
end
