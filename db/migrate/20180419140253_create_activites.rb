class CreateActivites < ActiveRecord::Migration[5.1]
  def change
    create_table :activites do |t|
      t.string :nom
      t.text :description
      t.integer :budget
      t.datetime :date_activite
      t.datetime :debut
      t.datetime :fin

      t.timestamps
    end
  end
end
