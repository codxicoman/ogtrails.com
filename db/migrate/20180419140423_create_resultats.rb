class CreateResultats < ActiveRecord::Migration[5.1]
  def change
    create_table :resultats do |t|
      t.integer :trimestre_un
      t.integer :trimestre_deux
      t.integer :trimestre_trois
      t.integer :trimestre_quatre
      #t.references :resultable, polymorphic: true

      t.timestamps
    end
  end
end
