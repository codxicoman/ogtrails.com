class CreateIndicateurs < ActiveRecord::Migration[5.1]
  def change
    create_table :indicateurs do |t|
      t.string :nom
      t.text :description

      t.timestamps
    end
  end
end
