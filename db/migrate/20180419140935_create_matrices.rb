class CreateMatrices < ActiveRecord::Migration[5.1]
  def change
    create_table :matrices do |t|
      t.string :annee
      t.integer :valeur_initiale
      t.integer :prevision
      t.integer :resultat
      t.integer :ecart
      t.references :indicateur, foreign_key: true

      t.timestamps
    end
  end
end
