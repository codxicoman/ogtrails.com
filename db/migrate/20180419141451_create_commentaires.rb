class CreateCommentaires < ActiveRecord::Migration[5.1]
  def change
    create_table :commentaires do |t|
      #t.references :user, foreign_key: true
      #t.references :matrice, foreign_key: true
      t.boolean :published
      t.datetime :published_at
      t.text :content

      t.timestamps
    end
  end
end
