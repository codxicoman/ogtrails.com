class CreateJoinTableMatricesActivites < ActiveRecord::Migration[5.1]
  def change
    create_join_table :matrices, :activites do |t|
      # t.index [:matrice_id, :activite_id]
      # t.index [:activite_id, :matrice_id]
    end
  end
end
