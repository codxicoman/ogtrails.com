class CreateBudgets < ActiveRecord::Migration[5.1]
  def change
    create_table :budgets do |t|
      t.integer :prevision
      t.datetime :trimestre1
      t.datetime :trimestre2
      t.datetime :trimestre3
      t.datetime :trimestre4
      t.integer :budgetable_id
      t.string :budgetable_type

      t.timestamps
    end
  end
end
