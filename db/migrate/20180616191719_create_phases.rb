class CreatePhases < ActiveRecord::Migration[5.1]
  def change
    create_table :phases do |t|
      t.boolean :activation
      t.datetime :date_debut
      t.datetime :date_fin

      t.timestamps
    end
  end
end
