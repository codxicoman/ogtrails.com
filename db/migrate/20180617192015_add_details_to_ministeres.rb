class AddDetailsToMinisteres < ActiveRecord::Migration[5.1]
  def change
    add_column :ministeres, :numero, :string
    add_column :ministeres, :email, :string
  end
end
