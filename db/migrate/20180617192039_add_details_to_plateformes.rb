class AddDetailsToPlateformes < ActiveRecord::Migration[5.1]
  def change
    add_column :plateformes, :numero, :string
    add_column :plateformes, :email, :string
  end
end
