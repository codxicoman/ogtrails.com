class AddDescriptionToPlateformes < ActiveRecord::Migration[5.1]
  def change
    add_column :plateformes, :description, :string
  end
end
