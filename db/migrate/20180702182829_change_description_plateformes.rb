class ChangeDescriptionPlateformes < ActiveRecord::Migration[5.1]
  def change
    change_column :plateformes, :description, :text
  end
end
