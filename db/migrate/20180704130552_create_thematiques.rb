class CreateThematiques < ActiveRecord::Migration[5.1]
  def change
    create_table :thematiques do |t|
      t.string :intitule

      t.timestamps
    end
  end
end
