class CreateThematiqueProgrammes < ActiveRecord::Migration[5.1]
  def change
    create_table :thematique_programmes do |t|
      t.references :programme, foreign_key: true
      t.references :thematique, foreign_key: true

      t.timestamps
    end
  end
end
