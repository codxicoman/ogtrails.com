class CreateThematiquePlateformes < ActiveRecord::Migration[5.1]
  def change
    create_table :thematique_plateformes do |t|
      t.references :plateforme, foreign_key: true
      t.references :thematique, foreign_key: true

      t.timestamps
    end
  end
end
