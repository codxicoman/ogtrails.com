class ChangeDescriptionForMinistere < ActiveRecord::Migration[5.1]
  def change
    change_column :ministeres, :description, :text
  end
end
