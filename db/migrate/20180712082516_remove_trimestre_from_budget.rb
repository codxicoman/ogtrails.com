class RemoveTrimestreFromBudget < ActiveRecord::Migration[5.1]
  def change
    remove_column :budgets, :trimestre1
    remove_column :budgets, :trimestre2
    remove_column :budgets, :trimestre3
    remove_column :budgets, :trimestre4
  end
end
