class AddTrimestreToBudget < ActiveRecord::Migration[5.1]
  def change
    add_column :budgets, :trimestre1, :integer
    add_column :budgets, :trimestre2, :integer
    add_column :budgets, :trimestre3, :integer
    add_column :budgets, :trimestre4, :integer
  end
end
