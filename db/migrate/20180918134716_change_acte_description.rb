class ChangeActeDescription < ActiveRecord::Migration[5.1]
  def change
    change_column :actes, :description, :text
  end
end
