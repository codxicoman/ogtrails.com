# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180918134716) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "actes", force: :cascade do |t|
    t.string "nom"
    t.text "description"
    t.bigint "programme_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["programme_id"], name: "index_actes_on_programme_id"
  end

  create_table "actions", force: :cascade do |t|
    t.string "nom"
    t.bigint "programme_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["programme_id"], name: "index_actions_on_programme_id"
  end

  create_table "activites", force: :cascade do |t|
    t.string "nom"
    t.text "description"
    t.datetime "date_activite"
    t.datetime "debut"
    t.datetime "fin"
    t.bigint "resultat_attenu_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["resultat_attenu_id"], name: "index_activites_on_resultat_attenu_id"
  end

  create_table "activites_matrices", id: false, force: :cascade do |t|
    t.bigint "matrix_id", null: false
    t.bigint "activite_id", null: false
  end

  create_table "budgets", force: :cascade do |t|
    t.integer "prevision"
    t.integer "budgetable_id"
    t.string "budgetable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "trimestre1"
    t.integer "trimestre2"
    t.integer "trimestre3"
    t.integer "trimestre4"
    t.index ["budgetable_id"], name: "index_budgets_on_budgetable_id"
    t.index ["budgetable_type"], name: "index_budgets_on_budgetable_type"
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.string "data_fingerprint"
    t.string "type", limit: 30
    t.integer "width"
    t.integer "height"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type"
  end

  create_table "commentaires", force: :cascade do |t|
    t.boolean "published"
    t.datetime "published_at"
    t.bigint "user_id"
    t.bigint "matrice_id"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["matrice_id"], name: "index_commentaires_on_matrice_id"
    t.index ["user_id"], name: "index_commentaires_on_user_id"
  end

  create_table "indicateurs", force: :cascade do |t|
    t.string "nom"
    t.text "description"
    t.bigint "activite_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["activite_id"], name: "index_indicateurs_on_activite_id"
  end

  create_table "matrices", force: :cascade do |t|
    t.string "annee"
    t.integer "valeur_initiale"
    t.integer "prevision"
    t.integer "resultat"
    t.integer "ecart"
    t.bigint "indicateur_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["indicateur_id"], name: "index_matrices_on_indicateur_id"
  end

  create_table "ministeres", force: :cascade do |t|
    t.string "nom"
    t.text "description"
    t.string "ministre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "numero"
    t.string "email"
  end

  create_table "periodes", force: :cascade do |t|
    t.string "libelle"
    t.datetime "date_debut"
    t.datetime "date_fin"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "phases", force: :cascade do |t|
    t.boolean "activation"
    t.datetime "date_debut"
    t.datetime "date_fin"
    t.bigint "programme_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["programme_id"], name: "index_phases_on_programme_id"
  end

  create_table "plateformes", force: :cascade do |t|
    t.string "nom"
    t.string "siege"
    t.bigint "ministere_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "numero"
    t.string "email"
    t.text "description"
    t.index ["ministere_id"], name: "index_plateformes_on_ministere_id"
  end

  create_table "programme_periodes", force: :cascade do |t|
    t.bigint "programme_id"
    t.bigint "periode_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["periode_id"], name: "index_programme_periodes_on_periode_id"
    t.index ["programme_id"], name: "index_programme_periodes_on_programme_id"
  end

  create_table "programmes", force: :cascade do |t|
    t.string "nom"
    t.bigint "ministere_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ministere_id"], name: "index_programmes_on_ministere_id"
  end

  create_table "resultat_attenus", force: :cascade do |t|
    t.bigint "acte_id"
    t.string "nom"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["acte_id"], name: "index_resultat_attenus_on_acte_id"
  end

  create_table "resultats", force: :cascade do |t|
    t.integer "trimestre_un"
    t.integer "trimestre_deux"
    t.integer "trimestre_trois"
    t.integer "trimestre_quatre"
    t.string "resultable_type"
    t.bigint "resultable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["resultable_type", "resultable_id"], name: "index_resultats_on_resultable_type_and_resultable_id"
  end

  create_table "roles", force: :cascade do |t|
    t.boolean "activation"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "thematique_plateformes", force: :cascade do |t|
    t.bigint "plateforme_id"
    t.bigint "thematique_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["plateforme_id"], name: "index_thematique_plateformes_on_plateforme_id"
    t.index ["thematique_id"], name: "index_thematique_plateformes_on_thematique_id"
  end

  create_table "thematique_programmes", force: :cascade do |t|
    t.bigint "programme_id"
    t.bigint "thematique_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["programme_id"], name: "index_thematique_programmes_on_programme_id"
    t.index ["thematique_id"], name: "index_thematique_programmes_on_thematique_id"
  end

  create_table "thematiques", force: :cascade do |t|
    t.string "intitule"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "titres", force: :cascade do |t|
    t.string "nom"
    t.bigint "ministere_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ministere_id"], name: "index_titres_on_ministere_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "prenom"
    t.string "nom"
    t.bigint "role_id"
    t.bigint "plateforme_id"
    t.boolean "ong3d"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["plateforme_id"], name: "index_users_on_plateforme_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["role_id"], name: "index_users_on_role_id"
  end

  add_foreign_key "actes", "programmes"
  add_foreign_key "activites", "resultat_attenus"
  add_foreign_key "matrices", "indicateurs"
  add_foreign_key "phases", "programmes"
  add_foreign_key "plateformes", "ministeres"
  add_foreign_key "programme_periodes", "periodes"
  add_foreign_key "programme_periodes", "programmes"
  add_foreign_key "programmes", "ministeres"
  add_foreign_key "resultat_attenus", "actes"
  add_foreign_key "thematique_plateformes", "plateformes"
  add_foreign_key "thematique_plateformes", "thematiques"
  add_foreign_key "thematique_programmes", "programmes"
  add_foreign_key "thematique_programmes", "thematiques"
  add_foreign_key "titres", "ministeres"
  add_foreign_key "users", "plateformes"
  add_foreign_key "users", "roles"
end
