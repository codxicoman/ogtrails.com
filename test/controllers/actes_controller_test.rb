require 'test_helper'

class ActesControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get actes_show_url
    assert_response :success
  end

  test "should get edit" do
    get actes_edit_url
    assert_response :success
  end

end
