require 'test_helper'

class GraphiquesControllerTest < ActionDispatch::IntegrationTest
  test "should get diagramme" do
    get graphiques_diagramme_url
    assert_response :success
  end

  test "should get courbe" do
    get graphiques_courbe_url
    assert_response :success
  end

end
