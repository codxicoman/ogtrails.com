require 'test_helper'

class IndicateursControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get indicateurs_show_url
    assert_response :success
  end

  test "should get edit" do
    get indicateurs_edit_url
    assert_response :success
  end

end
