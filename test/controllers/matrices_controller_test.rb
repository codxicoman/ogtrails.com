require 'test_helper'

class MatricesControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get matrices_show_url
    assert_response :success
  end

  test "should get edit" do
    get matrices_edit_url
    assert_response :success
  end

end
