require 'test_helper'

class PlateformesControllerTest < ActionDispatch::IntegrationTest
  test "should get edit" do
    get plateformes_edit_url
    assert_response :success
  end

  test "should get show" do
    get plateformes_show_url
    assert_response :success
  end

  test "should get destroy" do
    get plateformes_destroy_url
    assert_response :success
  end

  test "should get update" do
    get plateformes_update_url
    assert_response :success
  end

end
