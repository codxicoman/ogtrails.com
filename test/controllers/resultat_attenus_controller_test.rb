require 'test_helper'

class ResultatAttenusControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get resultat_attenus_show_url
    assert_response :success
  end

  test "should get edit" do
    get resultat_attenus_edit_url
    assert_response :success
  end

end
