require 'test_helper'

class TitresControllerTest < ActionDispatch::IntegrationTest
  setup do
    @titre = titres(:one)
  end

  test "should get index" do
    get titres_url
    assert_response :success
  end

  test "should get new" do
    get new_titre_url
    assert_response :success
  end

  test "should create titre" do
    assert_difference('Titre.count') do
      post titres_url, params: { titre: {  } }
    end

    assert_redirected_to titre_url(Titre.last)
  end

  test "should show titre" do
    get titre_url(@titre)
    assert_response :success
  end

  test "should get edit" do
    get edit_titre_url(@titre)
    assert_response :success
  end

  test "should update titre" do
    patch titre_url(@titre), params: { titre: {  } }
    assert_redirected_to titre_url(@titre)
  end

  test "should destroy titre" do
    assert_difference('Titre.count', -1) do
      delete titre_url(@titre)
    end

    assert_redirected_to titres_url
  end
end
